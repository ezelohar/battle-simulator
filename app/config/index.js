/**
 * Global config file
 */

var config = {
	ARMY_MIN_VALUE: 2,
	SQUADS_MIN_VALUE: 2,
	UNITS_MIN_VALUE:5,
	UNITS_MAX_VALUE: 10,
	ATTACK_STRATEGY_RANDOM: 0,
	ATTACK_STRATEGY_WEAKEST: 1,
	ATTACK_STRATEGY_STRONGEST: 2,
	SQUAD: {
		MIN: 2,
		ATTACK_STRATEGY: {
			RANDOM: 0,
			WEAKEST: 1,
			STRONGEST: 2
		}
	},
	SOLDIER: {
		EXPERIENCE: {
			MIN: 0,
			MAX: 50
		},
		MIN_RECHARGE_TIME: 200
	},
	VEHICLE: {
		OPERATORS: {
			MIN: 1,
			MAX: 3
		},
		RECHARGE: {
			MIN: 1000
		},
		VEHICLE_DAMAGE_RECEIVED: 60,
		MAIN_OPERATOR_DAMAGE_RECEIVED: 20
	},
	UNIT: {
		TYPE: {
			SOLDIER: 0,
			VEHICLE: 1
		},
		HEALTH: {
			MIN: 0,
			MAX: 5
		},
		RECHARGE: {
			MIN: 200,
			MAX: 2000
		}
	}
};

// Used so i don't need to type integers in json file and still be able to use integers later on
config.ATTACK_STRATEGY_FIXED = {
	random: config.ATTACK_STRATEGY_RANDOM,
	weakest: config.ATTACK_STRATEGY_WEAKEST,
	strongest: config.ATTACK_STRATEGY_STRONGEST
};

module.exports = config;
