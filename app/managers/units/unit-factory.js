'use strict';

var config = require('../../config');

var Soldier = require('./unit/soldier');
var Vehicle = require('./unit/vehicle');


const DEFAULT_UNIT_TYPE = config.UNIT.TYPE.SOLDIER;

class UnitFactory {
	/**
	 * Create a unit based on unit type
	 * @param type
	 * @returns {*}
	 */
	static createUnit (type) {
		if (!type) {
			type = DEFAULT_UNIT_TYPE;
		}

		if (type == config.UNIT.TYPE.SOLDIER) {
			return new Soldier();
		} else {
			return new Vehicle();
		}
	}
}


module.exports = UnitFactory;