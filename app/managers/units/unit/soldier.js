'use strict';

var UnitManager = require('../unit-manager');
var config = require('../../../config');

var _ = require('lodash');


const UNIT_TYPE = config.UNIT.TYPE.SOLDIER;

var SoldierManager = class SoldierManager extends UnitManager {
	/**
	 * Initialize soldier
	 */
	constructor () {
		super(UNIT_TYPE);
		this.properties = {};

		this.setDefaultValues();

		this.recalculateProperties();
	}

	/**
	 * Recalculate properties used in battle
	 */
	recalculateProperties () {
		this.properties.damage = this.calculateDamage();
	}

	/**
	 * Calculate damage dealt
	 * @returns {number}
	 */
	calculateDamage () {
		return 0.05 + this.experience/100;
	}

	/**
	 * Apply RECEIVED damage
	 * @param damage
	 */
	applyDamage (damage) {
		this.health = this.health - damage;

		this.recalculateProperties();
	}

	/**
	 * Calculate rate at which unit has chance to succesfully attack
	 * @returns {number}
	 */
	attackSuccessProbability () {
		return 0.5 * (1 + this.health/100) * _.random(50 + this.experience, 100) / 100;
	}

	/**
	 * Return damage dealt by unit
	 * @returns {number|*}
	 */
	unitDamage () {
		return this.properties.damage;
	}

	/**
	 * When unit does damage, we update it's experience
	 * @returns {number}
	 */
	doDamage () {
		this.updateExperience();
		return this.unitDamage();
	}

	/**
	 * Update unit experience
	 */
	updateExperience () {
		if (this.experience < 50) {
			this.experience++;
		}

		this.recalculateProperties();
	}
};


module.exports = SoldierManager;