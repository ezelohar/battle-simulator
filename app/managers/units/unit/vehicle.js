'use strict';

var config = require('../../../config');


var _ = require('lodash');

var UnitManager = require('../unit-manager');
var SoldierManager = require('./soldier');

const MIN_OPERATORS = config.VEHICLE.OPERATORS.MIN;
const MAX_OPERATORS = config.VEHICLE.OPERATORS.MAX;

const VEHICLE_DAMAGE_RECEIVED = config.VEHICLE.VEHICLE_DAMAGE_RECEIVED;
const MAIN_OPERATOR_DAMAGE_RECEIVED = config.VEHICLE.MAIN_OPERATOR_DAMAGE_RECEIVED;

var VehicleManager = class VehicleManager extends UnitManager {
	/**
	 * Initialize vehicle
	 */
	constructor () {
		super();
		this.operators = [];
		this.properties = {};


		this.generateOperators();
		this.setDefaultValues();

		this.recalculateProperties();
	};

	/**
	 * Generate soldiers to be operators
	 */
	generateOperators () {
		let numberOfOperators = _.random(MIN_OPERATORS, MAX_OPERATORS);
		for (let i = 0; i < numberOfOperators; i++ ) {
			this.operators.push(new SoldierManager());
		}
	};

	/**
	 * Recalculate properties used in battle
	 */
	recalculateProperties () {
		this.properties.damage = this.calculateDamage();
		this.properties.health = this.getTotalHealth();
	};

	/**
	 * Calculate damage dealt by vehicle
	 * @returns {number}
	 */
	calculateDamage () {
		let operatorsDamage = 0;
		let numberOfOperators = this.operators.length;
		for (let i = 0; i< numberOfOperators; i++) {
			operatorsDamage += this.operators[i].experience / 100;
		}

		return 0.1 + operatorsDamage;
	};

	/**
	 * Calculate vehicle health
	 * NOTE: Not sure if implemented as it should due to task understanding.
	 * @returns {number}
	 */
	calculateHealth () {
		let totalHealth = 0;
		let numberOfOperators = this.operators.length;
		for (let i = 0; i< numberOfOperators; i++) {
			totalHealth += this.operators[i].health;
		}

		//All operators are dead/inactive
		if (totalHealth === 0) {
			return 0;
		}

		totalHealth += this.health;
		return totalHealth/(numberOfOperators + 1);
	};

	/**
	 * Apply RECEIVED damage to vehicle and it operators
	 * @param damage
	 */
	applyDamage (damage) {
		let vehicleDamage = (VEHICLE_DAMAGE_RECEIVED / 100) * damage;
		let mainOperatorDamage = (MAIN_OPERATOR_DAMAGE_RECEIVED / 100) * damage;
		let restOfDamage = damage - (vehicleDamage + mainOperatorDamage);

		let numberOfOperators = this.operators.length;

		this.health -= vehicleDamage;

		if (numberOfOperators === 1) {
			this.operators[0].applyDamage(mainOperatorDamage + restOfDamage);
		} else {
			let randomOperator = _.random(this.MIN_OPERATORS, this.MAX_OPERATORS);
			let operatorDamage = restOfDamage / (numberOfOperators - 1);

			this.operators.forEach((operator, index) => {
				let damage = (index === randomOperator ? mainOperatorDamage : operatorDamage);
				operator.applyDamage(damage);
			});
		}

		// clear inactive operators
		let len = this.operators.length;
		while (len--) {
			if (!this.operators[len].isActive()) {
				this.operators.splice(len, 1);
			}
		}

		this.recalculateProperties();
	};

	/**
	 * Calculate attack success probability
	 * @returns {number}
	 */
	attackSuccessProbability () {
		let operatorsProbability = 1;
		let numberOfOperators = this.operators.length;
		for (let i = 0; i< numberOfOperators; i++) {
			operatorsProbability *= this.operators[i].attackSuccessProbability();
		}

		return 0.5 * (1 + this.properties.health/100) * Math.pow(operatorsProbability, 1/numberOfOperators);
	};

	/**
	 * return damage dealt by unit
	 * @returns {number}
	 */
	unitDamage () {
		return this.properties.damage;
	};

	/**
	 * do damage and update experience of the operators
	 * @returns {number}
	 */
	doDamage () {
		this.updateOperatorsExperience();
		return this.unitDamage();
	};

	/**
	 * Update operators experience
	 */
	updateOperatorsExperience () {
		this.operators.forEach(operator => {
			operator.updateExperience();
		});

		this.recalculateProperties();
	};

	/**
	 * Modify experience getter to return sum of operators experience due to fact that vehicle doesn't have exprience
	 * @returns {number}
	 */
	get experience () {
		let operatorsExperience = 0;
		this.operators.forEach(operator => {
			operatorsExperience += operator.experience;
		});

		return operatorsExperience/this.operators.length;
	}

	/**
	 * Check if vehicle is active
	 * @returns {boolean}
	 */
	isActive () {
		return (this.properties.health > 0 && this.operators.length > 0) ? true : false;
	}
};

module.exports = VehicleManager;