'use stric';

var config = require('../../config');
var _ = require('lodash');

const MIN_GENERATED_HEALTH = config.UNIT.HEALTH.MIN;
const MAX_GENERATED_HEALTH = config.UNIT.HEALTH.MAX;
const MIN_RECHARGE_TIME = config.UNIT.RECHARGE.MIN;
const MIN_RECHARGE_TIME_VEHICLE = config.VEHICLE.RECHARGE.MIN;
const MAX_RECHARGE_TIME = config.UNIT.RECHARGE.MAX;
const UNIT_TYPE_SOLDIER = config.UNIT.TYPE.SOLDIER;
const UNIT_TYPE_VEHICLE = config.UNIT.TYPE.VEHICLE;
const MIN_EXPERIENCE = config.SOLDIER.EXPERIENCE.MIN;
const MAX_EXPERIENCE = config.SOLDIER.EXPERIENCE.MAX;

var UnitManager = class UnitManager {
	/**
	 *
	 * @param {number} unitType | type of the unit which is being built
	 */
	constructor (unitType) {
		this.unitType = unitType;
	}

	/**
	 * Set unit default values
	 */
	setDefaultValues () {
		this.health = _.random(MIN_GENERATED_HEALTH, MAX_GENERATED_HEALTH);
		let minRechargeTime = (this.unitType === UNIT_TYPE_SOLDIER ? MIN_RECHARGE_TIME : MIN_RECHARGE_TIME_VEHICLE);
		this.recharge = _.random(minRechargeTime, MAX_RECHARGE_TIME);

		if (UNIT_TYPE_SOLDIER === this.unitType) {
			this.experience = _.random(this.MIN_EXPERIENCE, this.MAX_EXPERIENCE);
		}
	}

	/**
	 * check if unit is active
	 * @returns {boolean}
	 */
	isActive () {
		return (this.getTotalHealth() > 0) ? true : false;
	}

	/**
	 * return total health
	 * @returns {number}
	 */
	getTotalHealth () {
		return this.health;
	}
};


module.exports = UnitManager;