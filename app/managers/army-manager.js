
'use strict';

var SquadManager = require('./squad-manager');
var config = require('../config');

var _ = require('lodash');



var ArmyManager = class ArmyManager {
	/**
	 * Main constructor
	 * @param {object} army | main army information from config file
	 * @param {number} index | army index for identification
	 */
	constructor(army, index) {
		this.squads = [];
		this.index = index;
		this.armyName = `Army ${ index }`;

		console.log(`Building army with name "${ this.armyName }"`);

		this.buildSquads(army);
	};

	/**
	 * Build squads from army configuration
	 * @param {object} army
	 * @returns {boolean}
	 */
	buildSquads (army) {
		if (army.squads.length < config.SQUADS_MIN_VALUE) {
			console.log(`Squad count should be more than or equal to ${ config.SQUADS_MIN_VALUE }`);
			return false;
		}

		let self = this;

		army.squads.forEach((squad, index) => {
			let squadmg = new SquadManager(squad, self.index, index);
			squadmg.clearInactiveUnits();
			self.squads.push(squadmg);
		});
	};

	/**
	 * Check if army is active by having active squads
	 * @returns {boolean}
	 */
	isActive() {
		return (this.squads.length > 0) ? true : false;
	};

	/**
	 * Remove inactive army squads from simulation
	 */
	clearInactiveSquad () {
		let len = this.squads.length;
		while (len--) {
			if (!this.squads[len].isActive()) {
				this.squads.splice(len, 1);
			}
		}
	}

	/**
	 * Return current army weakest member
	 * @returns {object}
	 */
	getWeakestMember () {
		let weakestSquad = this.squads[0];
		let weakestSquadIndex = this.squads[0].calculateSquadStrength();
		this.squads.forEach(squad => {
			if (squad.isActive()) {
				if (weakestSquadIndex > squad.calculateSquadStrength()) {
					weakestSquadIndex = squad.calculateSquadStrength();
					weakestSquad = squad;
				}
			}
		});

		return weakestSquad;
	}

	/**
	 * Return current army strongest member
	 * @returns {object}
	 */
	getStrongestMember () {

		let strongestSquad = this.squads[0];
		let strongestSquadIndex = 0;
		this.squads.forEach(squad => {
			if (strongestSquadIndex < squad.calculateSquadStrength()) {
				strongestSquadIndex = squad.calculateSquadStrength();
				strongestSquad = squad;
			}
		});

		return strongestSquad;
	}

	/**
	 * Return a random squad member from the current army
	 * @returns {object}
	 */
	getRandomMember () {
		let randomSquad = _.random(0, this.squads.length -1);
		return this.squads[randomSquad];
	}
};

module.exports = ArmyManager;