'use stric';

var _ = require('lodash');
var Promise = require('bluebird');
var util = require('util');

var config = require('../config');
var UnitFactory = require('./units/unit-factory.js');
var BattleManager = require('./battle-manager');

const DEFAULT_ATTACK_TYPE = 0;

var SquadManager = class SquadManager {

	/**
	 * Main SquadManager constructor
	 * @param {array} squadData | Data about squad retrieved from config file
	 * @param {number} armyIndex | index of the army, squad belongs to
	 * @param {number} squadIndex | Identification of the squad
	 */
	constructor (squadData, armyIndex, squadIndex) {
		this.squadRechargeTime = 0;
		this.units = [];
		this.armyIndex = armyIndex;
		this.squadIndex = squadIndex;
		this.turns = 0;

		this.attackStrategy = (typeof squadData.attack_strategy !== 'undefined' ? config.ATTACK_STRATEGY_FIXED[squadData.attack_strategy] : DEFAULT_ATTACK_TYPE);

		this.build(squadData.units);
	};

	/**
	 * Build randomly squad units based on config setting
	 * @param {number} unitsCount
	 */
	build (unitsCount) {
		for (let i = 0; i < unitsCount; i++) {
			this.units.push(UnitFactory.createUnit(_.random(0, 1)));
		}
	};

	/**
	 * Validate attack and run charge.
	 */
	runAttack () {
		let squad = this;
		let rechargeTime = squad.calculateSquadRechargeTime();

		console.log(`Squad with armyIndex(${ squad.armyIndex }) and squadIndex(${ squad.squadIndex }) is recharging ${ rechargeTime } with ${ this.units.length } active units!`);

		if (squad.end()) {
			return;
		}

		squad.chargeAttack().then(() => {
			// Because of the charge time we need to check if there is still valid targets before attacking
			if (squad.end()) {
				return;
			}

			squad.doAttack().then(() => {
				//After attack run new attack process again
				squad.runAttack();
			});
		});
	}

	/**
	 * Charge squad attack
	 * @returns {bluebird|exports|module.exports}
	 */
	chargeAttack () {
		let self = this;
		let rechargeTime = self.calculateSquadRechargeTime();
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				resolve()
			}, rechargeTime);
		});
	}

	/**
	 * Execute attack on given target
	 * @returns {bluebird|exports|module.exports}
	 */
	doAttack () {
		let fromSquad = this;
		let attackStrategy = fromSquad.attackStrategy;

		console.log(`Squad with armyIndex: ${ fromSquad.armyIndex } and squadIndex: ${ fromSquad.squadIndex } is attacking with strategy ${ attackStrategy }!`);

		return new Promise((resolve, reject) => {
			let toSquad = {};
			switch (attackStrategy) {
				case config.SQUAD.ATTACK_STRATEGY.RANDOM:
					toSquad = BattleManager.getRandomSquad(fromSquad.armyIndex);
					break;
				case config.SQUAD.ATTACK_STRATEGY.WEAKEST:
					toSquad = BattleManager.getWeakestSquad(fromSquad.armyIndex);
					break;
				case config.SQUAD.ATTACK_STRATEGY.STRONGEST:
					toSquad = BattleManager.getStrongestSquad(fromSquad.armyIndex);
					break;
			}

			let fromSquadAttackProbability = fromSquad.attackSuccessProbability();
			let toSquadAttackProbability = toSquad.attackSuccessProbability();

			if (fromSquadAttackProbability > toSquadAttackProbability) {
				let damageDealt = fromSquad.doDamage();

				console.log(`Squad with armyIndex(${ fromSquad.armyIndex }) and squadIndex(${ fromSquad.squadIndex }) has dealt ${ damageDealt } damage to the squad with armyIndex(${ toSquad.armyIndex }) and squadIndex(${ toSquad.squadIndex }) with probability factor of ${ fromSquadAttackProbability } > ${ toSquadAttackProbability }!`);

				toSquad.applyDamage(damageDealt);
			} else {
				console.log(`Squad with armyIndex(${ fromSquad.armyIndex }) and squadIndex(${ fromSquad.squadIndex }) has failed attack to the squad with armyIndex(${ toSquad.armyIndex }) and squadIndex(${ toSquad.squadIndex })! with probability factor of ${ fromSquadAttackProbability } > ${ toSquadAttackProbability }`);
			}

			return resolve();
		});
	}

	/**
	 * Calculate squad attack success probability
	 * @returns {number}
	 */
	attackSuccessProbability () {
		let unitsProbability = 1;
		let numberOfUnits = this.units.length;
		for (let i = 0; i< numberOfUnits; i++) {
			unitsProbability += this.units[i].attackSuccessProbability();
		}

		return Math.pow(unitsProbability, 1/numberOfUnits);
	};

	/**
	 * Apply RECEIVED damage to the squad units
	 * @param {number} damage
	 */
	applyDamage (damage) {
		let numberOfUnits = this.units.length;
		let damagePerUnit = damage/numberOfUnits;
		for (let i = 0; i< numberOfUnits; i++) {
			this.units[i].applyDamage(damagePerUnit);
		}

		this.clearInactiveUnits();
	};

	/**
	 * Deal total squad damage to the enemy squad
	 * @returns {number}
	 */
	doDamage () {
		let unitsDamage = 0;
		let numberOfUnits = this.units.length;
		for (let i = 0; i< numberOfUnits; i++) {
			unitsDamage += this.units[i].doDamage();
		}

		return unitsDamage;
	};

	/**
	 * Squad strength is being calculated by taking average experience of it's members, average health, and average damage, and creating an index from it for comparison
	 * @returns {number}
	 */
	calculateSquadStrength () {
		let healthIndex = 0;
		let experienceIndex = 0;
		let damageIndex = 0;

		let numberOfUnits = this.units.length;

		this.units.forEach(unit => {
			healthIndex += unit.health;
			experienceIndex += unit.experience;
			damageIndex += unit.unitDamage();
		});

		healthIndex = healthIndex/numberOfUnits;
		experienceIndex = experienceIndex/numberOfUnits;
		damageIndex = damageIndex/numberOfUnits;

		return healthIndex + experienceIndex + damageIndex;
	}

	/**
	 * Calculate squad recharge time defined by inside units highest recharge time
	 * @returns {number}
	 */
	calculateSquadRechargeTime () {
		let max = 0;
		this.units.forEach(unit => {
			if (max < unit.recharge) {
				max = unit.recharge;
			}
		});

		this.squadRechargeTime = max;

		return max;
	}

	/**
	 * Clear units which aren't in the game any more
	 */
	clearInactiveUnits () {
		let len = this.units.length;
		while (len--) {
			if (!this.units[len].isActive()) {
				this.units.splice(len, 1);
			}
		}

		if (this.units.length === 0) {
			BattleManager.clearArmySquad(this.armyIndex);
		}
	}

	/**
	 * Return squad active status
	 * @returns {boolean}
	 */
	isActive () {
		return (this.units.length > 0) ? true : false;
	}

	/**
	 * End squad's life by declaring overall victory or given squad defeat
	 * @returns {boolean}
	 */
	end() {
		let squad = this;
		if (!squad.isActive()) {
			console.log(`Squad with armyIndex(${ squad.armyIndex }) and squadIndex(${ squad.squadIndex }) has DIED AND WON'T ATTACK ANYMORE!`);
			console.log('----------   UNIT HAS DIED   ----------');
			return true
		}

		if (!BattleManager.hasPlayers()) {
			console.log(`Squad with armyIndex(${ squad.armyIndex }) and squadIndex(${ squad.squadIndex }) has WON!`);
			console.log('----------   UNIT DOESN\'T HAVE NEW SQUAD TO ATTACK AND HAS WON    ----------');
			return true;
		}

		return false;
	}
};

module.exports = SquadManager;