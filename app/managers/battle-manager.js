'use strict';

var config = require('../config');

var util = require('util');
var _ = require('lodash');

var BattleManager = class BattleManager {
	constructors () {
		this.armies = [];
	}

	/**
	 * Store built armies object. Can be expanded in future to add new armies while simulation runs.
	 * @param {object} armies | Built armies object
	 */
	storeArmies (armies) {
		this.armies = armies;

		this.logArmies();
	}

	/**
	 * Just log armies
	 */
	logArmies () {
		console.log(util.inspect(this.armies, {showHidden: false, depth: null}), 'logArmies');
	}

	/**
	 * Run all squads attack at the same time
	 */
	runAttacks() {
		this.armies.forEach(army => {
			army.squads.forEach(squad => {
				squad.runAttack();
			})
		})
	}

	/**
	 * Retrieve weakest squad member from non attacking army
	 * @param {number} attackingArmyIndex | index of the army who attacks
	 * @returns {{}}
	 */
	getWeakestSquad (attackingArmyIndex) {
		let weakestSquad = {};
		let weakestSquadIndex = 9999;
		this.armies.forEach((army, index) => {
			if (army.isActive()) {
				if (index !== attackingArmyIndex) {
					let squad = army.getWeakestMember();
					if (weakestSquadIndex > squad.calculateSquadStrength()) {
						weakestSquadIndex = squad.calculateSquadStrength();
						weakestSquad = squad;
					}
				}
			}
		});

		return weakestSquad;
	}

	/**
	 * Retrieve strongest squad member from non attacking army
	 * @param {number} attackingArmyIndex | index of the army who attacks
	 * @returns {{}}
	 */
	getStrongestSquad (attackingArmyIndex) {
		let strongestSquad = {};
		let strongestSquadIndex = 0;
		this.armies.forEach((army, index) => {
			if (army.isActive()) {
				if (index !== attackingArmyIndex) {
					var squad = army.getStrongestMember();
					if (strongestSquadIndex < squad.calculateSquadStrength()) {
						strongestSquadIndex = squad.calculateSquadStrength();
						strongestSquad = squad;
					}
				}
			}
		});

		return strongestSquad;
	}

	/**
	 * Return random squad from non attacking army
	 * @param {number} attackingArmyIndex | index of the army who attacks
	 * @returns {*}
	 */
	getRandomSquad (attackingArmyIndex) {
		let validArmyIndexes = [];

		this.armies.forEach((army, index) => {
			if (army.armyIndex !== attackingArmyIndex && army.isActive()) {
				validArmyIndexes.push({validIndex: index});
			}
		});

		let armyIndex = validArmyIndexes[_.random(0, validArmyIndexes.length -1)].validIndex;

		return this.armies[armyIndex].getRandomMember();
	}

	/**
	 * Check to see if there are still valid squads to attack each other
	 * @returns {boolean}
	 */
	hasPlayers () {
		let hasPlayers = 0;
		this.armies.forEach((army, index) => {
			if (army.isActive()) {
				hasPlayers++;
			} else {

			}
		});

		console.log(`there are currently ${ hasPlayers } active`);

		return (hasPlayers > 1) ? true : false;
	}

	/**
	 * Remove invalid squad from the army
	 * @param (int) armyIndex | Index of the army we want to clear squads
	 */
	clearArmySquad (armyIndex) {
		this.logArmies();
		this.armies[armyIndex].clearInactiveSquad();
	}
};

module.exports = new BattleManager();