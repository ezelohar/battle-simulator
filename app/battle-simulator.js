'use strict';

var config = require('./config');
var ArmyManager = require('./managers/army-manager');
var BattleManager = require('./managers/battle-manager');

var BattleSimulator = class BattleSimulator {
	/**
	 * Run battle simulation
	 * @param {object} battleConfiguration
	 * @returns {void}
	 */
	runSimulation (battleConfiguration) {
		if (battleConfiguration.armies.length < config.ARMY_MIN_VALUE) {
			console.log(`Army count should be more than or equal to ${ config.ARMY_MIN_VALUE }`);
			return false;
		}

		let armies = this.build(battleConfiguration);

		BattleManager.storeArmies(armies);
		BattleManager.runAttacks();
	};

	/**
	 * Build all armies with their squads and units
	 * @param {object} battleConfiguration
	 * @returns {Array}
	 */
	build (battleConfiguration) {
		let armies = [];

		battleConfiguration.armies.forEach((army, index) => {
			armies.push(new ArmyManager(army, index));
		});

		return armies;
	};
};

module.exports = new BattleSimulator();