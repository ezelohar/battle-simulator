let battleConfiguration = require('./data/battle-configuration.json');
console.log('----------   LOAD ARMY CONFIGURATION   ----------');

let BattleSimulator = require('./app/battle-simulator');


console.log('Start simulation');
BattleSimulator.runSimulation(battleConfiguration);
